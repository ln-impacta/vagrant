#!/usr/bin/env bash

apt update && \
apt install -y mysql-server-5.7 && \
mysql < /vagrant/mysql/script/user.sql && \
cat /vagrant/mysql/mysqld.cnf > /etc/mysql/mysql.conf.d/mysqld.cnf && \
service mysql restart